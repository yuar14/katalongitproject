<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>section_Make Appointment                   _57f49e</name>
   <tag></tag>
   <elementGuidId>1b14bd5f-9cdf-47ad-9892-176ddf8c4332</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#appointment</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//section[@id='appointment']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>section</value>
      <webElementGuid>40203607-25b3-416b-8ec5-921e600ea398</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>appointment</value>
      <webElementGuid>18bf7242-4d33-436f-8f6b-a57aa2b0aba0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>section bg-primary</value>
      <webElementGuid>80a96ebc-ce13-4df9-89f1-c18b2dbfdeb1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
        
            
                Make Appointment
                
            
            
                
                    Facility
                    
                        
                            Tokyo CURA Healthcare Center
                            Hongkong CURA Healthcare Center
                            Seoul CURA Healthcare Center
                        
                    
                
                
                    
                        
                             Apply for hospital readmission
                        
                    
                
                
                    Healthcare Program
                    
                        
                             Medicare
                        
                        
                             Medicaid
                        
                        
                             None
                        
                    
                
                
                    Visit Date (Required)
                    
                        
                            
                            
                                
                            
                        
                    
                
                
                    Comment
                    
                        
                    
                
                
                    
                        Book Appointment
                    
                
            
        
    
</value>
      <webElementGuid>e2b3edd2-2e76-44c1-a203-06adf06295a8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;appointment&quot;)</value>
      <webElementGuid>fad8b4d7-3ca6-4f53-91c5-fddd38e40674</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//section[@id='appointment']</value>
      <webElementGuid>90139354-a58b-430f-b372-ebfc5b8b6c6e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Make Appointment'])[1]/following::section[1]</value>
      <webElementGuid>dceee062-7215-4a94-bd82-3761889425c5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='We Care About Your Health'])[1]/following::section[1]</value>
      <webElementGuid>d33330eb-ac27-407c-865e-d5b455d39d4e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section</value>
      <webElementGuid>a0201942-8827-4857-b493-481a8c61ef5e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//section[@id = 'appointment' and (text() = '
    
        
            
                Make Appointment
                
            
            
                
                    Facility
                    
                        
                            Tokyo CURA Healthcare Center
                            Hongkong CURA Healthcare Center
                            Seoul CURA Healthcare Center
                        
                    
                
                
                    
                        
                             Apply for hospital readmission
                        
                    
                
                
                    Healthcare Program
                    
                        
                             Medicare
                        
                        
                             Medicaid
                        
                        
                             None
                        
                    
                
                
                    Visit Date (Required)
                    
                        
                            
                            
                                
                            
                        
                    
                
                
                    Comment
                    
                        
                    
                
                
                    
                        Book Appointment
                    
                
            
        
    
' or . = '
    
        
            
                Make Appointment
                
            
            
                
                    Facility
                    
                        
                            Tokyo CURA Healthcare Center
                            Hongkong CURA Healthcare Center
                            Seoul CURA Healthcare Center
                        
                    
                
                
                    
                        
                             Apply for hospital readmission
                        
                    
                
                
                    Healthcare Program
                    
                        
                             Medicare
                        
                        
                             Medicaid
                        
                        
                             None
                        
                    
                
                
                    Visit Date (Required)
                    
                        
                            
                            
                                
                            
                        
                    
                
                
                    Comment
                    
                        
                    
                
                
                    
                        Book Appointment
                    
                
            
        
    
')]</value>
      <webElementGuid>40073d68-16a3-42c7-8b2a-43d32d6d6741</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
